/* Copyright (C) BrokeGames, Inc - All Rights Reserved
 * Unauthorized copying and modification, of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Steven Garcia <sjosegarcia23@gmail.com>, December 2015
 */
package net.brokegames.server.opcode;

import io.netty.buffer.ByteBuf;
import net.brokegames.server.Client;

/**
 * @author Steven
 *
 */
public interface ReceivePacketHandler {

	public void handlePacket(Client c, ByteBuf data);
	public boolean isLoggedIn(Client c);
}
