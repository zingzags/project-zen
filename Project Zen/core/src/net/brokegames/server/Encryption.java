/* Copyright (C) BrokeGames, Inc - All Rights Reserved
 * Unauthorized copying and modification, of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Steven Garcia <sjosegarcia23@gmail.com>, December 2015
 */
package net.brokegames.server;

import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
/**
 * @author Steven
 *
 */
public class Encryption {
	
	private Cipher cipher;
	private SecretKey secretKey;
	
	
	public Encryption() throws Exception {
		KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
		keyGenerator.init(128);
		secretKey = keyGenerator.generateKey();
		cipher = Cipher.getInstance("AES");
	}
	
	public byte[] encrypt(byte[] bytes) throws Exception {
		cipher.init(Cipher.ENCRYPT_MODE, secretKey);
		Base64.Encoder encoder = Base64.getEncoder();
		return encoder.encode(cipher.doFinal(bytes));
	}

	public byte[] decrypt(byte[] encryptedData) throws Exception {
		Base64.Decoder decoder = Base64.getDecoder();
		byte[] encryptedTextByte = decoder.decode(encryptedData);
		cipher.init(Cipher.DECRYPT_MODE, secretKey);
		return cipher.doFinal(encryptedTextByte);
	}
}
