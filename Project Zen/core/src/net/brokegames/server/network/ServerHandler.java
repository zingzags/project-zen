/* Copyright (C) BrokeGames, Inc - All Rights Reserved
 * Unauthorized copying and modification, of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Steven Garcia <sjosegarcia23@gmail.com>, December 2015
 */
package net.brokegames.server.network;

import java.util.Random;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerAdapter;
import io.netty.channel.ChannelHandlerContext;
import net.brokegames.server.Client;
import net.brokegames.server.opcode.PacketProcessor;
import net.brokegames.server.opcode.ReceivePacketHandler;

/**
 * @author Steven
 *
 */

public class ServerHandler extends ChannelHandlerAdapter {
	
	private int channel;
	private int world;
	private PacketProcessor processor;
	
	public ServerHandler(int world, int channel) {
		this.world = world;
		this.channel = channel;
		processor = PacketProcessor.getProcessor(world, channel);
	}
	
	@Override
	public void channelActive(ChannelHandlerContext ctx) {
		Client c = new Client(ctx.channel());
		Random r = new Random();
		c.setSessionId(r.nextLong());
		c.setWorld(world);
		c.setChannel(channel);
		ctx.attr(Client.CLIENT_KEY).set(c);
	}
	
	@Override
	public void channelInactive(ChannelHandlerContext ctx) {
		Client c = ctx.attr(Client.CLIENT_KEY).get();
		c.remove();
	}
	
	@Override
	public void channelRead(ChannelHandlerContext ctx, Object msg) {
		Client c = ctx.attr(Client.CLIENT_KEY).get();
		ByteBuf buffer = (ByteBuf) msg;
		if (buffer.readableBytes() < 1) {
			return;
		}
		short code = buffer.readShort();
		ReceivePacketHandler handler = processor.getHandler(code);
		if (handler != null && handler.isLoggedIn(c)) {
			handler.handlePacket(c, buffer);
		}
	}
	
	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {

	}
	
	@Override
	public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception {
		System.out.println("event");
	}
	
}
