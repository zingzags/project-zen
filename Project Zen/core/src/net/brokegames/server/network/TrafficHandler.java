/* Copyright (C) BrokeGames, Inc - All Rights Reserved
 * Unauthorized copying and modification, of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Steven Garcia <sjosegarcia23@gmail.com>, December 2015
 */
package net.brokegames.server.network;

import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.ChannelInitializer;
/**
 * @author Steven
 *
 */
public class TrafficHandler extends ChannelInitializer<SocketChannel> {

	
	private int world;
	private int channel;
	
	public TrafficHandler(int world, int channel) {
		this.world = world;
		this.channel = channel;
	}
	
	/* (non-Javadoc)
	 * @see io.netty.channel.ChannelInitializer#initChannel(io.netty.channel.Channel)
	 */
	
	@Override
	protected void initChannel(SocketChannel chan) throws Exception {
		ChannelPipeline pipeline = chan.pipeline();

		pipeline.addLast("decode", new ServerPacketDecoder());
		pipeline.addLast("encode", new ServerPacketEncoder());
		pipeline.addLast("handler", new ServerHandler(world, channel));
	}


}
