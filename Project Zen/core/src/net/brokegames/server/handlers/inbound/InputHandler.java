package net.brokegames.server.handlers.inbound;

import com.badlogic.gdx.math.Vector2;

import io.netty.buffer.ByteBuf;
import net.brokegames.server.Client;
import net.brokegames.server.opcode.AbstractPacketHandler;

public class InputHandler extends AbstractPacketHandler {

	@Override
	public void handlePacket(Client c, ByteBuf data) {	    
		data.readByte(); //direction
		Vector2 v = new Vector2(data.readFloat(), data.readFloat());
		c.getPlayer().getBody().applyLinearImpulse(v, c.getPlayer().getBody().getWorldCenter(), true);
	}	
}