package net.brokegames.server.handlers.outbound;

import com.badlogic.gdx.utils.Array;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import net.brokegames.server.Client;

public class SnapShot {

	private static Array<Client> connections = new Array<Client>();
	
	public static void sendGameState(ByteBuf packet) {
		for (Client client : connections) {
			client.write(packet);
		}
	}

	public static Array<Client> getConnections() {
		return connections;
	}
	
}
