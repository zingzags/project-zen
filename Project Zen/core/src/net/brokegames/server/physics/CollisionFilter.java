/* Copyright (C) BrokeGames, Inc - All Rights Reserved
 * Unauthorized copying and modification, of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Steven Garcia <sjosegarcia23@gmail.com>, December 2015
 */
package net.brokegames.server.physics;

/**
 * @author Steven
 *
 */
public class CollisionFilter {
	public static final short MAP_BOUNDS_FILTER = 1;
	public static final short PLAYER_FILTER = 2;
	public static final short MONSTER_FILTER = 3;
	public static final short MAP_OBJECT_FILTER = 4;
}
