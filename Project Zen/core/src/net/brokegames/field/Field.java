package net.brokegames.field;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;

import io.netty.buffer.ByteBuf;
import net.brokegames.server.Server;
import net.brokegames.server.component.FieldObject;
import net.brokegames.server.component.Mapper;
import net.brokegames.server.component.Player;
import net.brokegames.server.physics.CollisionFilter;

public class Field {

	private AtomicInteger runningOid = new AtomicInteger();
	private ConcurrentHashMap<Integer, Entity> entities = new ConcurrentHashMap<Integer, Entity>();

	public Field() {
        // Create our body definition
        BodyDef groundBodyDef = new BodyDef();  
        groundBodyDef.position.set(new Vector2(0, 15));  
        Body groundBody = Server.master.getWorld().createBody(groundBodyDef);  
        PolygonShape groundBox = new PolygonShape();  
        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = groundBox;
        fixtureDef.filter.categoryBits = CollisionFilter.MAP_BOUNDS_FILTER;
        fixtureDef.filter.maskBits = CollisionFilter.PLAYER_FILTER;
        groundBox.setAsBox(Gdx.graphics.getWidth(), .5f);
        groundBody.createFixture(groundBox, 0.0f); 
        groundBox.dispose();
	}
	
	/**
	 * This will add a new fieldObject, and at the same time 
	 * provide it with a fieldId
	 */
	public void addFieldObject(Entity entity) {
		FieldObject fObject = Mapper.fObj.get(entity);
		if (fObject != null) {
			fObject.setFieldId(runningOid.incrementAndGet());
		} else {
			fObject = new FieldObject(runningOid.incrementAndGet());
			entity.add(fObject);
		}
		entities.put(fObject.getFieldId(), entity);
	}
	
	/**
	 * This will remove the specified field object 
	 */
	public void removeFieldObject(Entity entity) {
		FieldObject fObject = Mapper.fObj.get(entity);
		if (fObject == null) {
			return;
		}
		entity.remove(FieldObject.class);
		runningOid.decrementAndGet();
		entities.remove(entity);
	}
	
	/**
	 * This will get the next free fieldId to use for the
	 * field object. 
	 */
	public int getNextObjectId() {
		return runningOid.incrementAndGet();
	}
	
	/**
	 * This method will broadcast a message to every client in the current field
	 * besides the one that gets passed through 
	 */
	public void writeRemote(Entity entity, ByteBuf packet) {
		if (packet == null) {
			return;
		}
		if (entity == null) {
			writeField(packet);
			return;
		}
		Player localPlayer = Mapper.pObj.get(entity);
		for (Entity player : entities.values()) {
			 Player remotePlayer = Mapper.pObj.get(player);
			 if (remotePlayer != null) {
				 if (localPlayer.getId() != remotePlayer.getId()) {
					 remotePlayer.getClient().write(packet);
				 }
			 }
		}
	}
	
	/**
	 * This method will write a packet to every client that is currently in the field 
	 */
	public void writeField(ByteBuf packet) {
		for (Entity enti : entities.values()) {
			 Player player = Mapper.pObj.get(enti);
			 if (player != null) {
				 player.getClient().write(packet);
			 }
		}
	}
}
