package net.brokegames.systems;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import net.brokegames.components.Player;
import net.brokegames.handlers.player.io.Direction;
import net.brokegames.logic.LocalClient;
import net.brokegames.opcode.Send;

public class InputMovementSystem extends EntitySystem {

	private Vector2 impluse;
	private Direction direction = null;
	
	public InputMovementSystem() {}
	
	@Override
	public void addedToEngine(Engine engine) {
	}

	@Override
	public void removedFromEngine(Engine engine) {}
	
	@Override
	public void update(float deltaTime) {
		Player player = LocalClient.getPlayer();
		Body body = player.getBody();
		Vector2 vel = body.getLinearVelocity();
		float velChange = 0;
		float impulseX  = 0;
		float impulseY = 0;

		if (Gdx.input.isKeyPressed(Keys.W)) {
			direction = Direction.UP;
			impulseY = body.getMass() * 2;
		}
		if (Gdx.input.isKeyPressed(Keys.A)) {
			direction = Direction.LEFT;
			velChange = -25 - vel.x;
			impulseX = body.getMass() * velChange;
		}
		if (Gdx.input.isKeyPressed(Keys.D)) {
			direction = Direction.RIGHT;
			velChange = 25 - vel.x;
			impulseX = body.getMass() * velChange;
		}
		if (direction != null) {
			impluse = new Vector2(impulseX, impulseY);
		    body.applyLinearImpulse(impluse, body.getWorldCenter(), true);
			send();
			direction = null;
			impluse = null;
		}
	}	
	
	public void send() {
        ByteBuf packet = Unpooled.buffer();
        packet.writeShort(Send.INPUT.getId());
        packet.writeByte(direction.getDirection());
        packet.writeFloat(impluse.x);
        packet.writeFloat(impluse.y);
        LocalClient.write(packet);
	}
}
