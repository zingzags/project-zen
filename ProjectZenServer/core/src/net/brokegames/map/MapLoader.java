/* Copyright (C) BrokeGames, Inc - All Rights Reserved
 * Unauthorized copying and modification, of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Steven Garcia <sjosegarcia23@gmail.com>, December 2015
 */
package net.brokegames.map;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapRenderer;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;

/**
 * @author Steven
 *
 */
public class MapLoader {

	private String map;
	private int mapId;
	private TiledMapRenderer tiledMapRenderer;
	private TiledMap tiledMap;
    
	public MapLoader(String map) {
		this.map = map;
		tiledMap = new TmxMapLoader().load(Gdx.files.internal("maps/" + map + ".tmx").toString());
		tiledMapRenderer = new OrthogonalTiledMapRenderer(tiledMap);
	}

	
	public TiledMap getTiledMap() {
		return tiledMap;
	}
	
	/**
	 * @return the map
	 */
	public String getMap() {
		return map;
	}

	/**
	 * @param map the map to set
	 */
	public void setMap(String map) {
		this.map = map;
	}

	/**
	 * @return the mapId
	 */
	public int getMapId() {
		return mapId;
	}

	/**
	 * @param mapId the mapId to set
	 */
	public void setMapId(int mapId) {
		this.mapId = mapId;
	}

	/**
	 * @return the tiledMapRenderer
	 */
	public TiledMapRenderer getTiledMapRenderer() {
		return tiledMapRenderer;
	}	
	
}
