/* Copyright (C) BrokeGames, Inc - All Rights Reserved
 * Unauthorized copying and modification, of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Steven Garcia <sjosegarcia23@gmail.com>, December 2015
 */
package net.brokegames.opcode;

import com.badlogic.gdx.utils.Array;

import net.brokegames.handlers.inbound.CreateBodyHandler;
import net.brokegames.handlers.inbound.GameStateHandler;

/**
 * @author Steven
 *
 */
public class PacketProcessor {

	private Array<Packet> packets;
	
	public PacketProcessor() {
		packets = new Array<Packet>();
	}
	
	public PacketHandler getHandler(short code) {
		for (Packet p : packets) {
			if (p.code == Receive.UNKNOWN.getRecById(code)) {
				return p.handler;
			}
		}
		return null;
	}
	
	public PacketProcessor getProcessor() {
		return this;
	}
	
	public void reset() {
		packets.clear();
		packets.add(new Packet(Receive.BODY, new CreateBodyHandler()));
		packets.add(new Packet(Receive.GAME_STATE, new GameStateHandler()));
	}
	
	public class Packet {
		
		public Receive code;
		public PacketHandler handler;
		
		public Packet(Receive gameState, PacketHandler handler) {
			this.code = gameState;
			this.handler = handler;
		}	
	}
	
}
