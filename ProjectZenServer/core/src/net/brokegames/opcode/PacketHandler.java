/* Copyright (C) BrokeGames, Inc - All Rights Reserved
 * Unauthorized copying and modification, of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Steven Garcia <sjosegarcia23@gmail.com>, December 2015
 */
package net.brokegames.opcode;

import io.netty.buffer.ByteBuf;

/**
 * @author Steven
 *
 */
public interface PacketHandler {

	public void handlePacket(ByteBuf data);
	public boolean isLoggedIn();
}
