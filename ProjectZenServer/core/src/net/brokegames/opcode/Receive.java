/* Copyright (C) BrokeGames, Inc - All Rights Reserved
 * Unauthorized copying and modification, of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Steven Garcia <sjosegarcia23@gmail.com>, December 2015
 */
package net.brokegames.opcode;

/**
 * @author Steven
 *
 */
public enum Receive {
	
	UNKNOWN(0x0),
	BODY(0x1),
	GAME_STATE(0x2),
	INPUT(0x3);
	
	
	private int code;
	
	private Receive(int code) {
		this.code = code;
	}
	
	public Receive getRecById(int code) {
		for (Receive r : Receive.values()) {
			if (r.code == code) {
				return r;
			}
		}
		return null;
	}
	
}
