/* Copyright (C) BrokeGames, Inc - All Rights Reserved
 * Unauthorized copying and modification, of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Steven Garcia <sjosegarcia23@gmail.com>, December 2015
 */
package net.brokegames.network;


import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerAdapter;
import io.netty.channel.ChannelHandlerContext;
import net.brokegames.logic.LocalClient;
import net.brokegames.opcode.PacketHandler;
import net.brokegames.opcode.PacketProcessor;

/**
 * @author Steven
 *
 */

public class ClientHandler extends ChannelHandlerAdapter {
	
	private PacketProcessor processor;
	
	public ClientHandler() {
		processor = new PacketProcessor();
		processor.reset();
	}
	
	@Override
	public void channelActive(ChannelHandlerContext ctx) {
		LocalClient.setConnection(ctx.channel());
	}
	
	@Override
	public void channelInactive(ChannelHandlerContext ctx) {
		
	}
	
	@Override
	public void channelRead(ChannelHandlerContext ctx, Object msg) {
		ByteBuf buffer = (ByteBuf) msg;
		if (buffer.readableBytes() < 1) {
			return;
		}
		short code = buffer.readShort();
		PacketHandler handler = processor.getHandler(code);
		if (handler != null && handler.isLoggedIn()) {
			handler.handlePacket(buffer);
		}
	}
		
	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
		
	}
	
	@Override
	public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception {

	}

}
