/* Copyright (C) BrokeGames, Inc - All Rights Reserved
 * Unauthorized copying and modification, of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Steven Garcia <sjosegarcia23@gmail.com>, December 2015
 */
package net.brokegames.network;

import com.badlogic.gdx.Gdx;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;

/**
 * @author Steven
 *
 */
public class ClientInitializer {

	
	
	private String host;
	private int port;
	private Bootstrap boot;
	private EventLoopGroup workerGroup;
	private Channel channel;
	
	public ClientInitializer(String host, int port) {
		this.host = host;
		this.port = port;
		boot = new Bootstrap();
		workerGroup = new NioEventLoopGroup();
	}
	
	
	public void start() {
		boot.group(workerGroup);
		boot.channel(NioSocketChannel.class);
		boot.option(ChannelOption.SO_KEEPALIVE, true);
		boot.handler(new ClientTrafficHandler());
		try {
			channel = boot.connect(host, port).sync().channel();
		} catch (Exception e) {
			Gdx.app.error("CONNECTION_ISSUE", "The client could not bind with the server.", e);
			return;
		}
		Gdx.app.log("CONNECTION_SUCCESS","A connection to the server has been established.");
	}
	
	public void close() {
		try {
			channel.close();
			workerGroup.shutdownGracefully();
		} catch(Exception e) {
			Gdx.app.error("SHUTDOWN_ISSUE", "There was an issue closing the connection to the server!", e);
		}
	}
}