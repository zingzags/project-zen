package net.brokegames.components;

import com.badlogic.ashley.core.ComponentMapper;

public class Mapper {
	public static ComponentMapper<FieldObject> fObj = ComponentMapper.getFor(FieldObject.class);
	public static ComponentMapper<Player> pObj = ComponentMapper.getFor(Player.class);

}
