package net.brokegames.client;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL30;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.utils.viewport.ExtendViewport;

import net.brokegames.handlers.outbound.PacketCreator;
import net.brokegames.logic.ClientConstants;
import net.brokegames.logic.LocalClient;
import net.brokegames.network.ClientInitializer;
import net.brokegames.world.GameWorld;

public class AppMain implements ApplicationListener {
	
	private ClientInitializer client;
	private OrthographicCamera camera;
	private ExtendViewport viewport;
	public static GameWorld game;
	
	@Override
	public void create () {
		Gdx.app.setLogLevel(Application.LOG_INFO);
		client = new ClientInitializer("127.0.0.1", 8484);
		client.start();
        camera = new OrthographicCamera();
        camera.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		viewport = new ExtendViewport(ClientConstants.WIDTH, ClientConstants.HEIGHT, camera);
		viewport.apply();
		game = new GameWorld();


        // Create our body definition
        BodyDef groundBodyDef = new BodyDef();  
        groundBodyDef.position.set(new Vector2(0, 15));  
        Body groundBody = GameWorld.getPhysics().createBody(groundBodyDef);  
        PolygonShape groundBox = new PolygonShape();
        groundBox.setAsBox(Gdx.graphics.getWidth(), .5f);
        groundBody.createFixture(groundBox, 0.0f); 
        groundBox.dispose();
        LocalClient.write(PacketCreator.sendBodyResquest());
	}

	@Override
	public void render () {
		Gdx.graphics.setTitle(ClientConstants.TITLE);
		Gdx.gl.glClear(GL30.GL_COLOR_BUFFER_BIT);
		Gdx.gl.glClearColor(0, 0, 0, 0);
		Gdx.gl.glBlendFunc(GL30.GL_SRC_ALPHA, GL30.GL_ONE_MINUS_SRC_ALPHA);
		Gdx.gl.glEnable(GL30.GL_BLEND);

		game.update(camera);
	}

	/* (non-Javadoc)
	 * @see com.badlogic.gdx.ApplicationListener#resize(int, int)
	 */
	@Override
	public void resize(int width, int height) {
		viewport.update(width, height, false);
		camera.position.set(0, 0, 0); //TODO Set this to relative to the players position when resizing the screen
	}

	/* (non-Javadoc)
	 * @see com.badlogic.gdx.ApplicationListener#pause()
	 */
	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see com.badlogic.gdx.ApplicationListener#resume()
	 */
	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see com.badlogic.gdx.ApplicationListener#dispose()
	 */
	@Override
	public void dispose() {
		client.close();
	}
}
