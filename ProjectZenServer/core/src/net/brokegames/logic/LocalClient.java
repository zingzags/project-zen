/* Copyright (C) BrokeGames, Inc - All Rights Reserved
 * Unauthorized copying and modification, of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Steven Garcia <sjosegarcia23@gmail.com>, December 2015
 */
package net.brokegames.logic;

import com.badlogic.ashley.core.Entity;

import io.netty.buffer.ByteBuf;
import io.netty.channel.Channel;
import io.netty.util.AttributeKey;
import net.brokegames.components.Player;

/**
 * @author Steven
 *
 */
public class LocalClient {

	public static final AttributeKey<LocalClient> KEY = AttributeKey.valueOf("LOCAL");
	private static Channel connection;
	private static long sessionId;
	private static int world;
	private static int channel;
	private static Entity entity;
	
	/**
	 * @return Gets the sessionId of the specified client
	 */
	public long getSessionId() {
		return sessionId;
	}

	/**
	 * @param Sets the sessionId of the client
	 */
	public static void setSessionId(long session) {
		sessionId = session;
	}
	
	/**
	 * @param The packet that is going to be sent to the "channel" which in this case means client
	 */	
	public static synchronized void write(ByteBuf packet) {
		connection.writeAndFlush(packet);
	}

	/**
	 * @return the channel index
	 */
	public static int getChannel() {
		return channel;
	}

	/**
	 * @param Sets the channel index
	 */
	public static void setChannel(int channelId) {
		channel = channelId;
	}

	/**
	 * @return the world index
	 */
	public static int getWorld() {
		return world;
	}

	/**
	 * @param Sets the world index
	 */
	public static void setWorld(int worldId) {
		world = worldId;
	}
	
	public static void setConnection(Channel connect) {
		connection = connect;
	}
	
	/**
	 * @return the connection
	 */
	public static Channel getConnection() {
		return connection;
	}
	
	public static boolean isLoggedIn() {
		return true; // for now
	}

	/**
	 * @return the player entity
	 */
	public static Entity getEntity() {
		return entity;
	}

	/**
	 * @param Setting the entity for the player
	 */
	public static void setEntity(Entity entity) {
		LocalClient.entity = entity;
	}
	
	/**
	 * @return the Player object 
	 */
	public static Player getPlayer() {
		if (entity == null) {
			return null;
		}
	    return entity.getComponent(Player.class);
	}
	
}
