package net.brokegames.handlers.outbound;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import net.brokegames.opcode.Send;

public class PacketCreator {

	public static ByteBuf sendBodyResquest() {
        ByteBuf packet = Unpooled.buffer();
        packet.writeShort(Send.BODY.getId());
        return packet;
	}
	
}
