package net.brokegames.handlers.outbound;

import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.utils.Array;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import net.brokegames.handlers.SendPacketHandler;
import net.brokegames.logic.LocalClient;
import net.brokegames.opcode.Send;
import net.brokegames.world.GameWorld;

public class WorldState implements SendPacketHandler {

	/**
	 * This is the class which will contain the current game state (snapshot) to send to the server.
	 * It is then the server's job to reconstruct it on their end. 
	 * 
	 * This class will contain the data compression algorithm, as well as all the data we will send down the pipeline
	 * 
	 */
	public WorldState() {}
	
	/**
	 * This method will send the current snapshot of the clients current game-state
	 * Which includes:
	 * - Players position / physics 
	 */
	public void encodeSnapShot() {
		if (LocalClient.getConnection() != null) {
			ByteBuf packet = Unpooled.buffer();
			packet.writeShort(Send.GAME_STATE.getId());
			Array<Body> bodies = new Array<Body>();
			GameWorld.getPhysics().getBodies(bodies);
			packet.writeShort(bodies.size);
			for (Body body : bodies) {
				packet.writeFloat(body.getPosition().x);
				packet.writeFloat(body.getPosition().y);
				packet.writeFloat(body.getLinearVelocity().x);
				packet.writeFloat(body.getLinearVelocity().y);
			}
			LocalClient.write(packet);
		}
	}

}
