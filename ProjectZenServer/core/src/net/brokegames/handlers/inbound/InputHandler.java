package net.brokegames.handlers.inbound;

import com.badlogic.gdx.math.Vector2;

import io.netty.buffer.ByteBuf;
import net.brokegames.logic.LocalClient;
import net.brokegames.opcode.AbstractPacketHandler;

public class InputHandler extends AbstractPacketHandler {
	
	/**
	 * This will handle the position data that comes back from the server
	 *  
	 */
	@Override
	public void handlePacket(ByteBuf data) {
		data.readInt();//fieldid
		data.readByte(); //direction
		Vector2 v = new Vector2(data.readFloat(), data.readFloat());
		LocalClient.getPlayer().getBody().applyLinearImpulse(v, LocalClient.getPlayer().getBody().getWorldCenter(), true);
	}
}
