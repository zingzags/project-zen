package net.brokegames.server.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

import net.brokegames.client.AppMain;
import net.brokegames.logic.ClientConstants;

public class DesktopLauncher {
	
	public static void main (String[] arg) {	
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.useGL30 = true;
		config.vSyncEnabled = true;
		config.fullscreen = ClientConstants.FULLSCREEN;
		config.useHDPI = false;
		if (!config.fullscreen) {
			config.width = ClientConstants.WIDTH;
			config.height = ClientConstants.HEIGHT;
		}
		new LwjglApplication(new AppMain(), config);
	}
	
}